from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    age = models.CharField(max_length=3, null=False ,blank=False, verbose_name='Возвраст')
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, verbose_name='Пол')
    CLIENT_TYPE_CHOICES = (
        ('person', 'Person'),
        ('company', 'Company'),
    )
    client_type = models.CharField(max_length=7, choices=CLIENT_TYPE_CHOICES, verbose_name='Тип клиента')
    phone_number = models.CharField(max_length=20)
    city_number = models.IntegerField(default=0, blank=True, null=True, verbose_name='Номер города')
    registration_date = models.DateField()
    address = models.CharField(max_length=255)
    avatar = models.ImageField(null=False, blank=True, upload_to='avatars', verbose_name='Аватар', default='avatars/default_profile.jpg')